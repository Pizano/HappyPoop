from bathrooms.models import Bathroom


def set_bathrooms():
    Bathroom.objects.update_or_create(bathroom='Hombres')
    Bathroom.objects.update_or_create(bathroom='Mujeres')
    Bathroom.objects.update_or_create(bathroom='Mixto')
