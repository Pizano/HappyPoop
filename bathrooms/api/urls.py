from django.conf.urls import url

from bathrooms.api.views import view_bathrooms, change_use, report_problem, view_problems, report_fix_problem

urlpatterns = [
    url(r'^view_bathrooms/$', view_bathrooms, name='api_view_bathrooms'),
    url(r'^change_use/$', change_use, name='api_change_use'),
    url(r'^report_problem/$', report_problem, name='api_report_problem'),
    url(r'^view_problems/$', view_problems, name='api_view_problems'),
    url(r'^report_fix_problem/$', report_fix_problem, name='api_report_fix_problem'),
]
