# -*- coding: utf-8 -*-

from datetime import datetime, timedelta

from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from bathrooms.models import Bathroom, Problem


@api_view(['POST', 'GET'])
def view_bathrooms(request):
    if request.method == 'GET':
        bathrooms_objects = Bathroom.objects.all()

        return Response({'bathrooms': bathrooms_objects.values()}, status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST', 'GET'])
@csrf_exempt
def change_use(request):
    if request.method == 'POST':

        try:
            bathroom = Bathroom.objects.get(pk=request.data['bathroom_id'])

            if bathroom.is_use:
                bathroom.is_use = False
                bathroom.save()
            else:
                bathroom.is_use = True
                bathroom.save()

            return Response(status=status.HTTP_200_OK)

        except Bathroom.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        except KeyError:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST', 'GET'])
@csrf_exempt
def report_problem(request):
    if request.method == 'POST':

        try:
            problem = request.data['problem']
            bathroom = Bathroom.objects.get(pk=request.data['bathroom_id'])

            Problem.objects.create(bathroom=bathroom, problem=problem)
            bathroom.is_working = False
            bathroom.save()

            return Response(status=status.HTTP_200_OK)

        except Bathroom.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        except KeyError:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST', 'GET'])
@csrf_exempt
def view_problems(request):
    if request.method == 'GET':
        problems = Problem.objects.all().order_by('-register_date')

        return Response({'problems': problems.values()}, status=status.HTTP_200_OK)

    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST', 'GET'])
@csrf_exempt
def report_fix_problem(request):
    if request.method == 'POST':

        try:
            problem = Problem.objects.get(pk=request.data['problem_id'])

            problem.is_fixed = True
            problem.save()

            bathroom = problem.bathroom
            bathroom.is_working = True
            bathroom.save()

            return Response(status=status.HTTP_200_OK)

        except Problem.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        except KeyError:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)
