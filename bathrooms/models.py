from django.db import models


# Create your models here.
class Bathroom(models.Model):
    bathroom = models.CharField(max_length=50)
    is_use = models.BooleanField(default=False)
    is_working = models.BooleanField(default=True)


class Problem(models.Model):
    bathroom = models.ForeignKey(Bathroom, on_delete=models.CASCADE)
    problem = models.TextField()
    register_date = models.DateTimeField(auto_now_add=True)
    is_fixed = models.BooleanField(default=False)
